#!/bin/bash

test_app_folder=/var/www/staging/master-79f3c408

# На момент активации /var/www/html указывает на предыдущую версию /master-2dca1fdf

# Перед активацией мы копируем в папку с новой версией символическую ссылку на предыдущую версию prev-1-version = /master-2dca1fdf
# cp /var/www/html $test_app_folder/prev-1-version || true

# Делаем символическую ссылку /var/www/html на новую промежуточную папку /master-79f3c408
# ln -fsnv $test_app_folder /var/www/html

# Откат на предыдущую версию - заменяем файл ссылки /var/www/html файлом ссылки на предыдущую версию, которая лежит в файле prev-1-version в папке с текущей версией
cp -Pv --remove-destination $test_app_folder/prev-1-version /var/www/html
